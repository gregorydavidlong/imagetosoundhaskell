{-# LANGUAGE FlexibleContexts #-}

module Main where

import qualified Codec.Picture            as Picture
import qualified System.Environment       as Env
import qualified System.Random            as Random
import qualified Vision.Image             as I
import qualified Vision.Image.JuicyPixels as JP

import qualified CommandLine              as CL
import qualified Midi                     as M

-- stack exec -- ImageToSoundHaskell-exe -i input_file -o output_file -a processing_algorithm
main :: IO ()
main = do
  randomGen <- Random.getStdGen
  argv <- Env.getArgs
  args <- CL.getArgs randomGen argv
  img <- loadImage $ CL.inputImagePath args
  print $ "Reading image from " ++ (CL.inputImagePath args)
  let notes = CL.midiProcessingAlgorithm args img
  let midiFile = M.makeMidiFile notes
  print $ "Writing MIDI to " ++ CL.outputMidiPath args
  M.writeMidiFile (CL.outputMidiPath args) midiFile

-- Read an image and convert it to a Friday-compatible format
loadImage :: FilePath -> IO I.RGB
loadImage imagePath = do
  imgOrError <- Picture.readImage imagePath
  return $
    case imgOrError of
      Left err -> error $ "Failed to read image: " ++ imagePath ++ " " ++ err
      Right dynamicImage -> JP.toFridayRGB $ Picture.convertRGB8 dynamicImage
