module BlackToVolumeProcessingAlgorithm
  ( blackToVolume
  ) where

import           Types                (ProcessingAlgorithm)

import           Data.Vector.Storable ((!))
import           Debug.Trace
import qualified Image                as I
import qualified Midi                 as M
import qualified Vision.Image         as VI
import qualified Vision.Primitive     as VP

type XPos = Int

type Height = Int

-- Process an image, with a given sample rate to reduce the size of the output, by looking at the amount of black
-- in each column and generating MIDI volume signals.
blackToVolume :: Int -> ProcessingAlgorithm
blackToVolume sampleRate image =
  pure $ map (toVolumeSignal maxPixelValue) $ zip xPositions pixelValues
  where
    VP.Z VP.:. height VP.:. width = VI.manifestSize image
    pixelValues =
      reverse $ map (sumPixelValues image height) $ sample xPositions
    maxPixelValue = maximum pixelValues
    xPositions = [0 .. (width - 1)]
    sample = filter (\x -> x `mod` sampleRate == 0)

-- For each x position in the image, sum each y position's pixel values
sumPixelValues :: VI.RGB -> XPos -> Height -> Int
sumPixelValues img height x = foldr combine 0 [0 .. (height - 1)]
  where
    combine y acc =
      let (r, g, b) = I.pixelAt img x y
      in r + g + b + acc

toVolumeSignal :: Int -> (XPos, Int) -> M.Signal
toVolumeSignal maxVal (x, v) =
  M.VolumeSignal
    M.Volume {M.volumePosition = x, M.volumeValue = normalizedVolume}
  where
    normalizedVolume =
      round $ (fromIntegral v / fromIntegral maxVal) * maxPossibleVolume
    maxPossibleVolume = 127
