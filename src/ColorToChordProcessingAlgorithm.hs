module ColorToChordProcessingAlgorithm
  ( colorToChord
  -- private
  , toNote
  ) where

import           Data.List            (transpose)
import           Data.Vector.Storable ((!))
import qualified Vision.Image         as VI
import qualified Vision.Primitive     as VP

import qualified Image                as I
import qualified Midi                 as M
import           Types                (ProcessingAlgorithm)

colorToChord :: ProcessingAlgorithm
colorToChord image =
  let VP.Z VP.:. height VP.:. width = VI.manifestSize image
      y = height `div` 2
      pxs = map (\x -> I.pixelAt image x y) [0 .. (width - 1)]
      toGScale = toNotes M.gMajor
  in transpose $ map toGScale pxs

{- Convert a RGB value to a list of three notes. -}
toNotes :: M.MidiScale -> (Int, Int, Int) -> [M.Signal]
toNotes scale (r, g, b) = [toNote scale r, toNote scale g, toNote scale b]

{- Convert a value (0-255) to a note. -}
toNote :: M.MidiScale -> Int -> M.Signal
toNote scale v =
  M.NoteSignal
    M.Note
    { M.noteStartPosition = 0
    , M.noteValue = fitToScale scale $ v `div` 2
    , M.noteLength = 8
    , M.noteVolume = 80
    }

{- Fit the note value into the nominated scale. -}
fitToScale :: M.MidiScale -> Int -> Int
fitToScale [s] val = s
fitToScale (s:scale) val
  | s < val = fitToScale scale val
  | s >= val = s
