module CommandLine
  ( getArgs
  , Args(..)
  ) where

import qualified System.Console.GetOpt   as Opt
import qualified System.Random           as Random

import qualified MidiProcessingAlgorithm as PA
import qualified Types                   as T

-- Command-line Arguments
data CmdLineFlag
  = InputImage String -- Input image file path
  | OutputMidi String -- Output midi file path
  | ProcessingAlgorithm String -- Algorithm to use for processing the image
  deriving (Show)

data Args = Args
  { inputImagePath          :: String
  , outputMidiPath          :: String
  , midiProcessingAlgorithm :: T.ProcessingAlgorithm
  }

getArgs :: Random.StdGen -> [String] -> IO Args
getArgs randomGen argv = do
  (flags, _vals) <- processArgs argv
  let imagePath = getInputImageFilePath flags
  let midiPath = getOutputMidiFilePath flags
  let pa = getMidiProcessingAlgorithm randomGen flags
  return $ Args imagePath midiPath pa

-- get the input image file path from the command-line args
getInputImageFilePath :: [CmdLineFlag] -> String
getInputImageFilePath (InputImage iip:_) = iip
getInputImageFilePath (_:xs) = getInputImageFilePath xs
getInputImageFilePath [] = error "Please provide an input image file path"

-- Get the output image file path from the command-line args
getOutputMidiFilePath :: [CmdLineFlag] -> String
getOutputMidiFilePath (OutputMidi omp:_) = omp
getOutputMidiFilePath (_:xs) = getOutputMidiFilePath xs
getOutputMidiFilePath [] = error "Please provide an output midi file path"

-- Get the Midi processing algorithm from the command-line args
getMidiProcessingAlgorithm ::
     Random.StdGen -> [CmdLineFlag] -> T.ProcessingAlgorithm
getMidiProcessingAlgorithm randomGen (ProcessingAlgorithm al:_) =
  PA.selectAlgorithm randomGen al
getMidiProcessingAlgorithm randomGen (_:xs) =
  getMidiProcessingAlgorithm randomGen xs
getMidiProcessingAlgorithm _ [] =
  error "Please provide a valid processing algorithm for the image"

options :: [Opt.OptDescr CmdLineFlag]
options =
  [ Opt.Option ['o'] ["output"] (Opt.ReqArg OutputMidi "FILE") "output FILE"
  , Opt.Option ['i'] ["input"] (Opt.ReqArg InputImage "FILE") "input FILE"
  , Opt.Option
      ['a']
      ["algorithm"]
      (Opt.ReqArg ProcessingAlgorithm "ALGORITHM")
      "processing-algorithm ALGORITHM"
  ]

processArgs :: [String] -> IO ([CmdLineFlag], [String])
processArgs argv =
  case Opt.getOpt Opt.Permute options argv of
    ([], _, _) -> ioError (userError $ Opt.usageInfo header options)
    ([_], _, _) -> ioError (userError $ Opt.usageInfo header options)
    (o, n, []) -> return (o, n)
    (_, _, errs) ->
      ioError (userError (concat errs ++ Opt.usageInfo header options))
  where
    header = "Usage: ImageToSoundHaskell [OPTION...] files..."
