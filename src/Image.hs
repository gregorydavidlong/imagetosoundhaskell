module Image
  ( pixelAt
  , extractPixelsFromImage
  , RGBColor
  , Height
  , Width
  ) where

import           Data.Vector.Storable ((!))
import qualified Vision.Image         as VI
import           Vision.Primitive

type RGBColor = (Int, Int, Int)

type Height = Int

type Width = Int

{- Get the RGB value for the pixel at the X/Y location in the image. -}
pixelAt :: VI.RGB -> Int -> Int -> (Int, Int, Int)
pixelAt img x y =
  let Z :. _height :. width = VI.manifestSize img
      px = VI.manifestVector img ! (x + (y * width))
  in ( fromIntegral (VI.rgbRed px)
     , fromIntegral (VI.rgbGreen px)
     , fromIntegral (VI.rgbBlue px))

extractPixelsFromImage :: VI.RGB -> Height -> Width -> [[RGBColor]]
extractPixelsFromImage image height width =
  map (\col -> map (pixelAt image col) yPositions) xPositions
  where
    xPositions = [0 .. (width - 1)]
    yPositions = [0 .. (height - 1)]
