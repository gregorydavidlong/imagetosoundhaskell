{-|
Module      : Midi
Description : For handling MIDI files.

Converts lists of notes into MIDI files.
-}
module Midi
  ( aMinor
  , chromatic
  , cMajor
  , cMajorArp
  , gMajor
  , makeMidiFile
  , writeMidiFile
  , MidiScale
  , Note(..)
  , Signal(..)
  , Volume(..)
  ) where

import qualified Codec.Midi as Midi

-- | MIDI notes are 0 (C) - 127 (G)
-- | Volumes are 0 - 100
data Note = Note
  { noteStartPosition :: Int
  , noteValue         :: Int
  , noteLength        :: Int
  , noteVolume        :: Int
  } deriving (Eq, Show)

data Volume = Volume
  { volumePosition :: Int
  , volumeValue    :: Int
  } deriving (Eq, Show)

data Signal
  = NoteSignal Note
  | VolumeSignal Volume
  deriving (Eq, Show)

type MidiScale = [Int]

-- MIDI --
-- | Make MIDI messages from a note
toNotes :: Signal -> [(Int, Midi.Message)]
toNotes (NoteSignal (Note start val len vol)) =
  [(start, Midi.NoteOn 0 val vol), (len, Midi.NoteOff 0 val 0)]
toNotes (VolumeSignal (Volume pos vol)) = [(pos, Midi.ControlChange 0 11 vol)]

-- | Make a track from a list of notes.
makeTrack :: [Signal] -> [(Int, Midi.Message)]
makeTrack = concatMap toNotes

-- | Make a MIDI file from the list of notes
makeMidiFile :: [[Signal]] -> Midi.Midi
makeMidiFile notes =
  let tracks = map makeTrack notes
  in Midi.Midi
     { Midi.fileType = Midi.MultiTrack
     , Midi.timeDiv = Midi.TicksPerBeat 24
     , Midi.tracks = tracks
     }

-- | Write a MIDI file to the provided path
writeMidiFile :: FilePath -> Midi.Midi -> IO ()
writeMidiFile = Midi.exportFile

-- | Chromatic scale
chromatic :: MidiScale
chromatic = [0 .. 127]

-- | Midi values for A minor scale
aMinor :: MidiScale
aMinor =
  [ a0
  , b0
  , c1
  , d1
  , e1
  , f1
  , g1
  , a1
  , b1
  , c2
  , d2
  , e2
  , f2
  , g2
  , a2
  , b2
  , c3
  , d3
  , e3
  , f3
  , g3
  , a3
  , b3
  , c4
  , d4
  , e4
  , f4
  , g4
  , a4
  , b4
  , c5
  , d5
  , e5
  , f5
  , g5
  , a5
  , b5
  , c6
  , d6
  , e6
  , f6
  , g6
  , a6
  , b6
  , c7
  , d7
  , e7
  , f7
  , g7
  , a7
  , b7
  , c8
  , d8
  ]

-- | Midi values for C major scale
cMajor :: MidiScale
cMajor =
  [ 0
  , 2
  , 4
  , 5
  , 7
  , 9
  , 11
  , c0
  , d0
  , e0
  , f0
  , g0
  , a0
  , b0
  , c1
  , d1
  , e1
  , f1
  , g1
  , a1
  , b1
  , c2
  , d2
  , e2
  , f2
  , g2
  , a2
  , b2
  , c3
  , d3
  , e3
  , f3
  , g3
  , a3
  , b3
  , c4
  , d4
  , e4
  , f4
  , g4
  , a4
  , b4
  , c5
  , d5
  , e5
  , f5
  , g5
  , a5
  , b5
  , c6
  , d6
  , e6
  , f6
  , g6
  , a6
  , b6
  , c7
  , d7
  , e7
  , f7
  , g7
  , a7
  , b7
  , c8
  , d8
  , 112
  , 113
  , 115
  , 117
  , 119
  , 120
  , 122
  , 124
  , 125
  , 127
  ]

-- Major C Arppegio
cMajorArp :: MidiScale
cMajorArp =
  [ 0
  , 4
  , 7
  , c0
  , e0
  , g0
  , c1
  , e1
  , g1
  , c2
  , e2
  , g2
  , c3
  , e3
  , g3
  , c4
  , e4
  , g4
  , c5
  , e5
  , g5
  , c6
  , e6
  , g6
  , c7
  , e7
  , g7
  , c8
  , 112
  , 115
  , 120
  , 124
  , 127
  ]

-- | Midi values for G major scale. G, A, B, C, D, E, and F♯
gMajor :: MidiScale
gMajor =
  [ 7
  , 9
  , 11
  , c0
  , d0
  , e0
  , fs0_gb0
  , g0
  , a0
  , b0
  , c1
  , d1
  , e1
  , fs1_gb1
  , g1
  , a1
  , b1
  , c2
  , d2
  , e2
  , fs2_gb2
  , g2
  , a2
  , b2
  , c3
  , d3
  , e3
  , fs3_gb3
  , g3
  , a3
  , b3
  , c4
  , d4
  , e4
  , fs4_gb4
  , g4
  , a4
  , b4
  , c5
  , d5
  , e5
  , fs5_gb5
  , g5
  , a5
  , b5
  , c6
  , d6
  , e6
  , fs6_gb6
  , g6
  , a6
  , b6
  , c7
  , d7
  , e7
  , fs7_gb7
  , g7
  , a7
  , b7
  , c8
  , d8
  ]

-- | MIDI note values
c0 = 12

cs0_db0 = 13

d0 = 14

ds0_eb0 = 15

e0 = 16

f0 = 17

fs0_gb0 = 18

g0 = 19

gs0_ab0 = 20

a0 = 21

as0_bb0 = 22

b0 = 23

c1 = 24

cs1_db1 = 25

d1 = 26

ds1_eb1 = 27

e1 = 28

f1 = 29

fs1_gb1 = 30

g1 = 31

gs1_ab1 = 32

a1 = 33

as1_bb1 = 34

b1 = 35

c2 = 36

cs2_db2 = 37

d2 = 38

ds2_eb2 = 39

e2 = 40

f2 = 41

fs2_gb2 = 42

g2 = 43

gs2_ab2 = 44

a2 = 45

as2_bb2 = 46

b2 = 47

c3 = 48

cs3_db3 = 49

d3 = 50

ds3_eb3 = 51

e3 = 52

f3 = 53

fs3_gb3 = 54

g3 = 55

gs3_ab3 = 56

a3 = 57

as3_bb3 = 58

b3 = 59

c4 = 60

cs4_db4 = 61

d4 = 62

ds4_eb4 = 63

e4 = 64

f4 = 65

fs4_gb4 = 66

g4 = 67

gs4_ab4 = 68

a4 = 69

as4_bb4 = 70

b4 = 71

c5 = 72

cs5_db5 = 73

d5 = 74

ds5_eb5 = 75

e5 = 76

f5 = 77

fs5_gb5 = 78

g5 = 79

gs5_ab5 = 80

a5 = 81

as5_bb5 = 82

b5 = 83

c6 = 84

cs6_db6 = 85

d6 = 86

ds6_eb6 = 87

e6 = 88

f6 = 89

fs6_gb6 = 90

g6 = 91

gs6_ab6 = 92

a6 = 93

as6_bb6 = 94

b6 = 95

c7 = 96

cs7_db7 = 97

d7 = 98

ds7_eb7 = 99

e7 = 100

f7 = 101

fs7_gb7 = 102

g7 = 103

gs7_ab7 = 104

a7 = 105

as7_bb7 = 106

b7 = 107

c8 = 108

cs8_db8 = 109

d8 = 110

ds8_eb8 = 111
