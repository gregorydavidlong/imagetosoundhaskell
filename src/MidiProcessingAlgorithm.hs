module MidiProcessingAlgorithm
  ( selectAlgorithm
    -- for testing
  , calculateRandomCircleCoords
  , circle
  , expandingCircles
  , normalizeToScale
  , mergeIntoChords
  , shuffleToChord
  ) where

import qualified Data.Vector.Storable              as Vec
import qualified Image                             as I
import qualified System.Random                     as Random
import qualified System.Random.Shuffle             as Shuffle
import qualified Vision.Image                      as VI
import qualified Vision.Primitive                  as VP

import qualified BlackToVolumeProcessingAlgorithm  as BTV
import qualified ColorToChordProcessingAlgorithm   as CTC
import qualified Midi                              as M
import qualified PathToMelodyProcessingAlgorithm   as PTM
import qualified TracedToMelodyProcessingAlgorithm as Traced
import           Types                             (ProcessingAlgorithm)

type RGBColor = (Int, Int, Int)

processingAlgorithmScale = M.aMinor

selectAlgorithm :: Random.StdGen -> String -> ProcessingAlgorithm
selectAlgorithm randomGen "expanding-circles" = expandingCircles randomGen
selectAlgorithm randomGen "shuffle-to-chord" = shuffleToChord randomGen
selectAlgorithm _randomGen "traced-to-melody" = Traced.tracedToMelody
selectAlgorithm _randomGen "color-to-chord" = CTC.colorToChord
selectAlgorithm _randomGen "black-to-volume" = BTV.blackToVolume 12
selectAlgorithm _randomGen "path-to-melody" =
  PTM.pathToMelodyProcessingAlgorithm
selectAlgorithm _ _ =
  error "Please provide a valid processing algorithm for the image"

-- | Expanding Circles Processing Algorithm
expandingCircles :: Random.RandomGen t => t -> ProcessingAlgorithm
expandingCircles randomGen img =
  let VP.Z VP.:. height VP.:. width = VI.manifestSize img
      maxCircleRadius = 8 :: Int
      pixelCoords =
        calculateRandomCircleCoords randomGen maxCircleRadius width height
      pixelAtTuple (x, y) = I.pixelAt img x y
      calculatePixelVal (r, g, b) = r + g + b
      bluePixelVals =
        map (map (map (calculatePixelVal . pixelAtTuple))) pixelCoords
      flattenedCircles = map (map sum) bluePixelVals
      positionsInScale =
        normalizeToScale (length processingAlgorithmScale - 1) $
        concat flattenedCircles
      vals =
        map (\pos -> head $ drop pos processingAlgorithmScale) positionsInScale
      notes =
        concatMap
          (\(index, val) ->
             [ M.NoteSignal
                 M.Note
                 { M.noteStartPosition =
                     maxCircleRadius - (index `mod` maxCircleRadius)
                 , M.noteValue = val
                 , M.noteLength = (index `mod` maxCircleRadius) + 24
                 , M.noteVolume = 80
                 }
             ]) $
        zip [0 ..] vals
      mergedIntoChords = mergeIntoChords maxCircleRadius notes
  in mergedIntoChords

shuffleToChord :: Random.RandomGen t => t -> ProcessingAlgorithm
shuffleToChord randomGen img =
  let shuffledPixels = shufflePixelsInImg randomGen img
      redNotes = toNotes $ map first shuffledPixels
      greenNotes = toNotes $ map second shuffledPixels
      blueNotes = toNotes $ map third shuffledPixels
  in [redNotes, greenNotes, blueNotes]

first (a, _, _) = a

second (_, b, _) = b

third (_, _, c) = c

-- Expanding Circles
type Point = (Float, Float)

type Polygon = [Point]

-- | Put notes in chords
mergeIntoChords :: Int -> [M.Signal] -> [[M.Signal]]
mergeIntoChords chordSize notes =
  let notesAssignedToChords = zip [x `mod` chordSize | x <- [0 ..]] notes
  in map
       (\chordNumToBuild ->
          map snd $ -- Just take the note
          filter
            (\(currentChordNum, note) -> chordNumToBuild == currentChordNum)
            notesAssignedToChords)
       [0 .. (chordSize - 1)]

calculateRandomCircleCoords ::
     (Integral t2, Integral t3, Integral t, Integral t1, Random.RandomGen t4)
  => t4
  -> Int
  -> t3
  -> t2
  -> [[[(t1, t)]]]
calculateRandomCircleCoords randomGen maxRadius width height =
  let numNotesInPhrase = 128
      xToShuffle = [maxRadius .. (fromIntegral width - maxRadius)]
      yToShuffle = [maxRadius .. (fromIntegral height - maxRadius)]
      shuffledX =
        map fromIntegral $
        Shuffle.shuffle' xToShuffle (length xToShuffle) randomGen :: [Float]
      shuffledY =
        map fromIntegral $
        Shuffle.shuffle' yToShuffle (length yToShuffle) randomGen :: [Float]
      centres = zip shuffledX shuffledY
      droplets =
        map
          (\(x, y) ->
             map (circle (x, y)) [1 .. (fromIntegral maxRadius :: Float)])
          centres
  in take numNotesInPhrase $
     map (map (map (\(x, y) -> (round x, round y)))) droplets

-- where scale is the max normalized value, e.g. normalize values in xs into a number between 0..scale.
normalizeToScale :: Int -> [Int] -> [Int]
normalizeToScale scale xs =
  normalizeToScaleAlt scale xs (minimum xs) (maximum xs)

normalizeToScaleAlt :: Int -> [Int] -> Int -> Int -> [Int]
normalizeToScaleAlt scale [] _min _max = []
normalizeToScaleAlt scale (x:xs) minn maxx =
  normalized : normalizeToScaleAlt scale xs minn maxx
  where
    normalized =
      round ((fromIntegral (x - minn) / fromIntegral maxx) * fromIntegral scale)

-- Circle with eight "corners"
circle :: Point -> Float -> Polygon
circle (x, y) r =
  take 8 $ map (\t -> (x + r * cos t, y + r * sin t)) [0,0.2 .. (2 * pi)]

--------
toNotes :: [Int] -> [M.Signal]
toNotes =
  map
    (\p ->
       M.NoteSignal
         M.Note
         { M.noteStartPosition = 0
         , M.noteValue = div p 2
         , M.noteLength = 8
         , M.noteVolume = 80
         })

shufflePixelsInImg :: Random.RandomGen t => t -> VI.RGB -> [RGBColor]
shufflePixelsInImg randomGen img =
  let VP.Z VP.:. height VP.:. width = VI.manifestSize img
      shuffledX = Shuffle.shuffle' [0 .. (width - 1)] width randomGen :: [Int]
      shuffledY = Shuffle.shuffle' [0 .. (height - 1)] height randomGen :: [Int]
      pixelsToSelect = [(x, y) | x <- shuffledX, y <- shuffledY] :: [(Int, Int)]
  in map (pixelAtTuple img) pixelsToSelect

pixelAtTuple :: VI.RGB -> (Int, Int) -> RGBColor
pixelAtTuple img (x, y) = I.pixelAt img x y
