module PathToMelodyProcessingAlgorithm
  ( pathToMelodyProcessingAlgorithm
  ) where

import qualified Data.HashMap     as Map
import qualified Data.Set         as Set
import qualified Image            as I
import qualified Midi             as M
import           Types            (ProcessingAlgorithm)
import qualified Vision.Image     as VI
import qualified Vision.Primitive as VP

type YPosition = Int

pathToMelodyProcessingAlgorithm :: ProcessingAlgorithm
pathToMelodyProcessingAlgorithm image =
  let VP.Z VP.:. height VP.:. width = VI.manifestSize image
      pixels = I.extractPixelsFromImage image height width
      uniqueColors = Set.elems $ findUniqueColors pixels
      paths = map (followColorPath pixels) uniqueColors
  in map (mergeDuplicates . toNotes height) paths

toNotes :: Int -> [(Int, Int)] -> [M.Signal]
toNotes maxVal [] = []
toNotes maxVal ((_x, y):ps) =
  M.NoteSignal
    M.Note
    { M.noteStartPosition = 0
    , M.noteValue = fitToScale M.cMajor $ 127 - (normalize maxVal y)
    , M.noteLength = 1
    , M.noteVolume = 80
    } :
  toNotes maxVal ps

mergeDuplicates :: [M.Signal] -> [M.Signal]
mergeDuplicates = foldr merge []
  where
    merge n [] = [n]
    merge (M.NoteSignal n) acc@((M.NoteSignal a):as)
      | M.noteValue a == M.noteValue n =
        M.NoteSignal
          M.Note
          { M.noteStartPosition = M.noteStartPosition n
          , M.noteValue = M.noteValue n
          , M.noteLength = M.noteLength n + M.noteLength a
          , M.noteVolume = M.noteVolume n
          } :
        as
      | otherwise = (M.NoteSignal n) : acc

fitToScale :: M.MidiScale -> Int -> Int
fitToScale (s:[]) val = s
fitToScale (s:scale) val
  | s < val = fitToScale scale val
  | s >= val = s

followColorPath :: [[I.RGBColor]] -> I.RGBColor -> [(Int, Int)]
followColorPath pixels color =
  let startPosition = findStartPosition pixels color 0
  in followColorPath' pixels color startPosition [startPosition]

followColorPath' ::
     [[I.RGBColor]] -> I.RGBColor -> (Int, Int) -> [(Int, Int)] -> [(Int, Int)]
followColorPath' pixels color (cx, cy) currentPath =
  let left = (colorAt pixels (cx - 1, cy), (cx - 1, cy))
      right = (colorAt pixels (cx + 1, cy), (cx + 1, cy))
      up = (colorAt pixels (cx, cy + 1), (cx, cy + 1))
      down = (colorAt pixels (cx, cy - 1), (cx, cy - 1))
      upLeft = (colorAt pixels (cx - 1, cy + 1), (cx - 1, cy + 1))
      upRight = (colorAt pixels (cx + 1, cy + 1), (cx + 1, cy + 1))
      downLeft = (colorAt pixels (cx - 1, cy - 1), (cx - 1, cy - 1))
      downRight = (colorAt pixels (cx + 1, cy - 1), (cx + 1, cy - 1))
      ps = [left, right, up, down, upLeft, upRight, downLeft, downRight]
      ps' = removeInvalid color currentPath ps
      newPos = choose ps'
  in case newPos of
       Just pos -> followColorPath' pixels color pos (currentPath ++ [(cx, cy)])
       Nothing -> currentPath

removeInvalid ::
     I.RGBColor
  -> [(Int, Int)]
  -> [(Maybe I.RGBColor, (Int, Int))]
  -> [(Int, Int)]
removeInvalid _color _currentPath [] = []
removeInvalid color currentPath ((Nothing, _):candidates) =
  removeInvalid color currentPath candidates
removeInvalid color currentPath ((Just color', pos):candidates) =
  if color /= color'
    then removeInvalid color currentPath candidates
    else if elem pos currentPath
           then removeInvalid color currentPath candidates
           else (pos : (removeInvalid color currentPath candidates))

colorAt :: [[I.RGBColor]] -> (Int, Int) -> Maybe I.RGBColor
colorAt [] _              = Nothing
colorAt [[]] _            = Nothing
colorAt ((p:_):_) (0, 0)  = Just p
colorAt ((_:ys):_) (0, y) = colorAt [ys] (0, y - 1)
colorAt (ys:xs) (x, y)    = colorAt xs (x - 1, y)

choose :: [(Int, Int)] -> Maybe (Int, Int)
choose []    = Nothing
choose (x:_) = Just x

findStartPosition :: [[I.RGBColor]] -> I.RGBColor -> Int -> (Int, Int)
findStartPosition [] _ _ = error "Could not find color."
findStartPosition (col:rows) color xPos =
  case findMinPos col color 0 of
    Just yPos -> (xPos, yPos)
    Nothing   -> findStartPosition rows color (xPos + 1)

findMinPos :: [I.RGBColor] -> I.RGBColor -> Int -> Maybe Int
findMinPos [] _ _ = Nothing
findMinPos (px:pxs) color yPos =
  if px == color
    then Just yPos
    else findMinPos pxs color (yPos + 1)

-- Get a set of all the unique colors in the image (R, G, B)
findUniqueColors :: [[I.RGBColor]] -> Set.Set I.RGBColor
findUniqueColors pixels =
  let zippedPixels = map (zip [0 ..]) pixels :: [[(YPosition, I.RGBColor)]]
      pixelsToColIndexMap = map columnToMap zippedPixels
      columnToMap = foldr toMap Map.empty
      toMap (index, (r, g, b)) = Map.insertWith min (r, g, b) index
  in Set.filter (\x -> x /= (255, 255, 255)) $
     Set.unions $ map Map.keysSet pixelsToColIndexMap

normalize :: Int -> Int -> Int
normalize maxVal val = div (val * 103) maxVal
