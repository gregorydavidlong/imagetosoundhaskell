module TracedToMelodyProcessingAlgorithm
  ( tracedToMelody
    -- private
  , columnToMap
  ) where

import qualified Data.Vector.Storable  as Vec
import qualified Vision.Image          as VI
import qualified Vision.Image.RGB.Type as RGB
import qualified Vision.Primitive      as VP

import qualified Midi                  as M
import           Types                 (ProcessingAlgorithm)

import qualified Data.HashMap          as Map
import qualified Data.Maybe            as Maybe
import qualified Data.Set              as Set
import qualified Image                 as I

type RGBColor = (Int, Int, Int)

type Height = Int

type Width = Int

-- Position in a column. 0 == top-most pixel position
type YPosition = Int

-- Position in a row. 0 == left-most pixel position
type XPosition = Int

processingAlgorithmScale = M.cMajor

tracedToMelody :: ProcessingAlgorithm
tracedToMelody image =
  let VP.Z VP.:. height VP.:. width = VI.manifestSize image
      pixels = I.extractPixelsFromImage image height width
      zippedPixels = map (zip [0 ..]) pixels :: [[(YPosition, RGBColor)]]
      pixelsToColIndexMap =
        map columnToMap zippedPixels :: [Map.Map RGBColor YPosition]
      -- Get a set of all the unique colors in the image (R, G, B)
      uniqueColors =
        Set.unions $ map Map.keysSet pixelsToColIndexMap :: Set.Set RGBColor
      -- For each color, determine the top position for each column
      positionsForColors =
        getTopPositions uniqueColors pixelsToColIndexMap :: [[Maybe.Maybe YPosition]]
      normalizedPositions = map (map (normalize height)) positionsForColors
      positionsForColorsFitToScale =
        map (map (fitToScale processingAlgorithmScale)) normalizedPositions
      notes =
        map (mergeDuplicates . toNotes height) positionsForColorsFitToScale
  in notes

mergeDuplicates = foldr merge []
  where
    merge n [] = [n]
    merge (M.NoteSignal n) acc@((M.NoteSignal a):as)
      | M.noteValue a == M.noteValue n =
        M.NoteSignal
          M.Note
          { M.noteStartPosition = M.noteStartPosition n
          , M.noteValue = M.noteValue n
          , M.noteLength = M.noteLength n + M.noteLength a
          , M.noteVolume = M.noteVolume n
          } :
        as
      | otherwise = (M.NoteSignal n) : acc

normalize :: Int -> Maybe Int -> Maybe Int
normalize maxVal Nothing    = Nothing
normalize maxVal (Just val) = Just (div (val * 103) maxVal)

fitToScale :: M.MidiScale -> Maybe Int -> Maybe Int
fitToScale _scale Nothing = Nothing
fitToScale (s:[]) val = Just s
fitToScale (s:scale) (Just val)
  | s < val = fitToScale scale $ Just val
  | s >= val = Just s

toNotes :: Height -> [Maybe YPosition] -> [M.Signal]
toNotes maxVal = concatMap $ toNote maxVal

toNote :: Height -> Maybe YPosition -> [M.Signal]
toNote _maxVal Nothing =
  [ M.NoteSignal
      M.Note
      { M.noteStartPosition = 0
      , M.noteValue = 0
      , M.noteLength = 8
      , M.noteVolume = 0
      }
  ]
toNote maxVal (Just val) =
  [ M.NoteSignal
      M.Note
      { M.noteStartPosition = 0
      , M.noteValue = 127 - val
      , M.noteLength = 8
      , M.noteVolume = 80
      }
  ]

getTopPositions ::
     Set.Set RGBColor
  -> [Map.Map RGBColor YPosition]
  -> [[Maybe.Maybe YPosition]]
getTopPositions colorSet colMaps = map getTopPositionsForColor colorList
  where
    colorList = Set.elems colorSet
    getTopPositionsForColor color = map (Map.lookup color) colMaps

columnToMap :: [(YPosition, RGBColor)] -> Map.Map RGBColor YPosition
columnToMap = foldr toMap Map.empty
  where
    toMap (index, (r, g, b)) = Map.insertWith min (r, g, b) index
