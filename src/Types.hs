module Types
  ( ProcessingAlgorithm
  ) where

import qualified Midi         as M
import qualified Vision.Image as I

type ProcessingAlgorithm = I.RGB -> [[M.Signal]]
