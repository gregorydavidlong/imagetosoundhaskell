module BlackToVolumeProcessingAlgorithmSpec
  ( main
  , spec
  ) where

import           Test.Hspec

import qualified Codec.Picture                    as Picture
import qualified Midi                             as M
import qualified Vision.Image                     as I
import qualified Vision.Image.JuicyPixels         as JP

import qualified BlackToVolumeProcessingAlgorithm as BTV

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "blackToVolume" $ do
    it "creates volume changes" $ do
      img <- loadImage $ testImagePath
      (BTV.blackToVolume 1 img) `shouldBe`
        [ [ M.VolumeSignal (M.Volume {M.volumePosition = 0, M.volumeValue = 0})
          , M.VolumeSignal (M.Volume {M.volumePosition = 1, M.volumeValue = 64})
          , M.VolumeSignal
              (M.Volume {M.volumePosition = 2, M.volumeValue = 127})
          ]
        ]

testImagePath :: FilePath
testImagePath = "images/8-black-to-volume-test.png"

loadImage :: FilePath -> IO I.RGB
loadImage imagePath = do
  imgOrError <- Picture.readImage imagePath
  return
    (case imgOrError of
       Left err -> error ("Failed to read image: " ++ imagePath ++ " " ++ err)
       Right dynamicImage -> JP.toFridayRGB $ Picture.convertRGB8 dynamicImage)
