module ColorToChordProcessingAlgorithmSpec
  ( main
  , spec
  ) where

import           Test.Hspec
import           Test.Hspec.QuickCheck
import           Test.QuickCheck.Arbitrary       (arbitrary)
import           Test.QuickCheck.Gen             (Gen, listOf, suchThat)
import           Test.QuickCheck.Property        (forAll)

import qualified Midi                            as M

import qualified ColorToChordProcessingAlgorithm as CTC

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "toNote" $ do
    it "creates a note" $ do
      (CTC.toNote M.gMajor 0) `shouldBe`
        (M.NoteSignal $
         M.Note
         { M.noteStartPosition = 0
         , M.noteValue = (M.gMajor !! 0)
         , M.noteLength = 8
         , M.noteVolume = 80
         })
    prop "note noteValue of 0 is the first note of the scale" $
      forAll genScale $ \scale ->
        ((CTC.toNote scale 0) ==
         (M.NoteSignal $
          M.Note
          { M.noteStartPosition = 0
          , M.noteValue = (scale !! 0)
          , M.noteLength = 8
          , M.noteVolume = 80
          }))

genNoteVal :: Gen Int
genNoteVal =
  fmap (\x -> x `mod` 256) (arbitrary :: Gen Int) `suchThat`
  (\v -> (v >= 0) && (v <= 255))

genScale :: Gen [Int]
genScale = listOf genNoteVal `suchThat` (\s -> length s > 0)
