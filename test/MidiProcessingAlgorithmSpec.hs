module MidiProcessingAlgorithmSpec
  ( main
  , spec
  ) where

import           Test.Hspec

import qualified Codec.Picture            as Picture
import qualified System.Random            as Random
import qualified Vision.Image             as I
import qualified Vision.Image.JuicyPixels as JP

import qualified Midi                     as M
import qualified MidiProcessingAlgorithm  as PA

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "calculateRandomCircleCoords" $ do
    it "has circles with eight corners" $ do
      randomGen <- Random.getStdGen
      let edgeNoteLength = 30 :: Int
      (length
         (head $
          head $
          PA.calculateRandomCircleCoords
            randomGen
            1
            edgeNoteLength
            edgeNoteLength)) `shouldBe`
        8
  describe "circle" $ do
    it "runs without crashing" $ do
      (head $ PA.circle (1, 1) 1) `shouldBe` (2, 1)
  describe "expandingCircles" $ do
    it "creates eight tracks of notes" $ do
      randomGen <- Random.getStdGen
      img <-
        loadImage $
        "/Users/gregorydavidlong/development/ImageToSoundHaskell/images/4-Paul-Signac.jpg"
      (length $ PA.expandingCircles randomGen img) `shouldBe` 8
  describe "normalizeToScale" $ do
    it "fits noteValues into scale" $ do
      PA.normalizeToScale 5 [1, 100] `shouldBe` [0, 5]
      PA.normalizeToScale 10 [0, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100] `shouldBe`
        [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
  describe "mergeIntoChords" $ do
    it "puts eight notes into eight chords" $ do
      PA.mergeIntoChords
        8
        [ makeNote 1
        , makeNote 2
        , makeNote 3
        , makeNote 4
        , makeNote 5
        , makeNote 6
        , makeNote 7
        , makeNote 8
        ] `shouldBe`
        [ [makeNote 1]
        , [makeNote 2]
        , [makeNote 3]
        , [makeNote 4]
        , [makeNote 5]
        , [makeNote 6]
        , [makeNote 7]
        , [makeNote 8]
        ]
    it "puts four notes into two chords" $ do
      PA.mergeIntoChords 2 [makeNote 1, makeNote 2, makeNote 3, makeNote 4] `shouldBe`
        [[makeNote 1, makeNote 3], [makeNote 2, makeNote 4]]

-- TODO: Copied from Main
loadImage :: FilePath -> IO I.RGB
loadImage imagePath = do
  imgOrError <- Picture.readImage imagePath
  return
    (case imgOrError of
       Left err -> error ("Failed to read image: " ++ imagePath ++ " " ++ err)
       Right dynamicImage -> JP.toFridayRGB $ Picture.convertRGB8 dynamicImage)

makeNote :: Int -> M.Signal
makeNote x =
  M.NoteSignal $
  M.Note
  {M.noteStartPosition = x, M.noteValue = x, M.noteLength = x, M.noteVolume = x}
