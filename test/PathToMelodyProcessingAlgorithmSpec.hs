module PathToMelodyProcessingAlgorithmSpec
  ( main
  , spec
  ) where

import           Test.Hspec

import qualified Codec.Picture                   as Picture
import           Midi
import qualified Vision.Image                    as I
import qualified Vision.Image.JuicyPixels        as JP

import           PathToMelodyProcessingAlgorithm (pathToMelodyProcessingAlgorithm)

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

spec :: Spec
spec = do
  describe "pathToMelody" $ do
    it "creates a melody from test image" $ do
      img <- loadImage testImagePath
      (pathToMelodyProcessingAlgorithm img) `shouldBe`
        [ [ NoteSignal
              (Note
               { noteStartPosition = 0
               , noteValue = 50
               , noteLength = 2
               , noteVolume = 80
               })
          , NoteSignal
              (Note
               { noteStartPosition = 0
               , noteValue = 76
               , noteLength = 1
               , noteVolume = 80
               })
          , NoteSignal
              (Note
               { noteStartPosition = 0
               , noteValue = 50
               , noteLength = 1
               , noteVolume = 80
               })
          , NoteSignal
              (Note
               { noteStartPosition = 0
               , noteValue = 76
               , noteLength = 1
               , noteVolume = 80
               })
          , NoteSignal
              (Note
               { noteStartPosition = 0
               , noteValue = 103
               , noteLength = 1
               , noteVolume = 80
               })
          , NoteSignal
              (Note
               { noteStartPosition = 0
               , noteValue = 127
               , noteLength = 1
               , noteVolume = 80
               })
          ]
        ]

testImagePath :: FilePath
testImagePath = "images/9-path-to-melody-test.png"

loadImage :: FilePath -> IO I.RGB
loadImage imagePath = do
  imgOrError <- Picture.readImage imagePath
  return
    (case imgOrError of
       Left err -> error ("Failed to read image: " ++ imagePath ++ " " ++ err)
       Right dynamicImage -> JP.toFridayRGB $ Picture.convertRGB8 dynamicImage)
