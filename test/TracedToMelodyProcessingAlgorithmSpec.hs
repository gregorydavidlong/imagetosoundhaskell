module TracedToMelodyProcessingAlgorithmSpec
  ( main
  , spec
  ) where

import           Test.Hspec

import qualified Codec.Picture                     as Picture
import qualified Data.HashMap                      as Map
import qualified Image                             as I
import qualified Midi                              as M
import qualified Vision.Image                      as VI
import qualified Vision.Image.JuicyPixels          as JP

import qualified TracedToMelodyProcessingAlgorithm as Traced

-- `main` is here so that this module can be run from GHCi on its own.  It is
-- not needed for automatic spec discovery.
main :: IO ()
main = hspec spec

testImagePath :: String
testImagePath = "images/5-traced3-test.jpeg"

spec :: Spec
spec = do
  describe "tracedToMelody" $ do
    it "processed test input" $ do
      img <- loadImage $ testImagePath
      Traced.tracedToMelody img `shouldBe`
        [ [ M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 92
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          ]
        , [ M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 41
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 16
            , M.noteVolume = 0
            }
          ]
        , [ M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 75
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          ]
        , [ M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 110
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          ]
        , [ M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 58
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          ]
        , [ M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 127
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 92
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          ]
        , [ M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 127
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 0
            , M.noteLength = 8
            , M.noteVolume = 0
            }
          ]
        , [ M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 110
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 41
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          , M.NoteSignal $
            M.Note
            { M.noteStartPosition = 0
            , M.noteValue = 127
            , M.noteLength = 8
            , M.noteVolume = 80
            }
          ]
        ]
    describe "columnToMap" $ do
      it "finds the maximum position for each color" $ do
        img <- loadImage $ testImagePath
        let firstColumn =
              [ (0, I.pixelAt img 0 0)
              , (1, I.pixelAt img 0 1)
              , (2, I.pixelAt img 0 2)
              , (3, I.pixelAt img 0 3)
              , (4, I.pixelAt img 0 4)
              , (5, I.pixelAt img 0 5)
              ]
        Traced.columnToMap firstColumn `shouldBe`
          Map.fromList
            [((254, 6, 6), 5), ((255, 7, 217), 0), ((255, 255, 255), 1)]

-- TODO: Copied from Main
loadImage :: FilePath -> IO VI.RGB
loadImage imagePath = do
  imgOrError <- Picture.readImage imagePath
  return
    (case imgOrError of
       Left err -> error ("Failed to read image: " ++ imagePath ++ " " ++ err)
       Right dynamicImage -> JP.toFridayRGB $ Picture.convertRGB8 dynamicImage)
